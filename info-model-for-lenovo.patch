From cb0c026adad0b3c892d3bb84366ed871d920e97d Mon Sep 17 00:00:00 2001
From: Felipe Borges <felipeborges@gnome.org>
Date: Tue, 28 Apr 2020 12:32:05 +0200
Subject: [PATCH] info: Add device "Model" row for Lenovo

Since we will be shipping Fedora 32 in some Lenovo devices[0], we
were asked to add an entry in Settings with a string identifying
the laptop model for users to look at while reporting issues[1].

Ideally this logic should live in systemd-hostnamed and be generic
enough to handle vendors and their particular ways of exposing this
info in userspace. Unfortunately the release is at the door now and
that work would require us some more time. Its development is ongoing
upstream[2] but won't likely be ready for the F32 that should happen
at any moment this week.

[0] https://fedoramagazine.org/coming-soon-fedora-on-lenovo-laptops
[1] https://teams.fedoraproject.org/project/silverblue/us/127
[2] https://github.com/systemd/systemd/issues/15493 and
https://github.com/systemd/systemd/pull/15531
---
 panels/info-overview/cc-info-overview-panel.c | 42 +++++++++++++++++++
 .../info-overview/cc-info-overview-panel.ui   |  9 ++++
 2 files changed, 51 insertions(+)

diff --git a/panels/info-overview/cc-info-overview-panel.c b/panels/info-overview/cc-info-overview-panel.c
index 98c77e97f..1b9916ac7 100644
--- a/panels/info-overview/cc-info-overview-panel.c
+++ b/panels/info-overview/cc-info-overview-panel.c
@@ -62,6 +62,7 @@ struct _CcInfoOverviewPanel
   CcListRow       *disk_row;
   CcListRow       *gnome_version_row;
   CcListRow       *graphics_row;
+  CcListRow       *device_model_row;
   GtkListBox      *hardware_box;
   GtkDialog       *hostname_editor;
   CcHostnameEntry *hostname_entry;
@@ -417,6 +418,41 @@ get_graphics_hardware_string (void)
   return g_strdup (renderer);
 }
 
+static gchar *
+get_device_model_string (void)
+{
+  g_autofree char *vendor = NULL;
+  g_autoptr(GError) error = NULL;
+  gchar *model = NULL;
+
+  if (g_file_get_contents ("/sys/class/dmi/id/sys_vendor", &vendor, NULL, &error))
+    {
+      if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_NOENT))
+        return NULL;
+
+      vendor = g_strchomp (vendor);
+      if ((g_strcmp0 (vendor, "LENOVO") != 0) && (g_strcmp0 (vendor, "lenovo") != 0))
+        return NULL;
+
+      if (g_file_get_contents ("/sys/class/dmi/id/product_version", &model, NULL, &error))
+        {
+          if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_NOENT))
+            return NULL;
+
+          model = g_strchomp (model);
+          if (g_strcmp0 (model, "") == 0) {
+            g_free (model);
+
+            return NULL;
+          }
+
+          return model;
+        }
+    }
+
+    return NULL;
+}
+
 static char *
 get_os_name (void)
 {
@@ -692,6 +728,7 @@ info_overview_panel_setup_overview (CcInfoOverviewPanel *self)
   g_autofree char *os_type_text = NULL;
   g_autofree char *os_name_text = NULL;
   g_autofree gchar *graphics_hardware_string = NULL;
+  g_autofree gchar *device_model_string = NULL;
 
   if (load_gnome_version (&gnome_version, NULL, NULL))
     cc_list_row_set_secondary_label (self->gnome_version_row, gnome_version);
@@ -717,6 +754,10 @@ info_overview_panel_setup_overview (CcInfoOverviewPanel *self)
 
   graphics_hardware_string = get_graphics_hardware_string ();
   cc_list_row_set_secondary_markup (self->graphics_row, graphics_hardware_string);
+
+  device_model_string = get_device_model_string ();
+  cc_list_row_set_secondary_markup (self->device_model_row, device_model_string);
+  gtk_widget_set_visible (GTK_WIDGET (self->device_model_row), (device_model_string != NULL));
 }
 
 static gboolean
@@ -819,6 +860,7 @@ cc_info_overview_panel_class_init (CcInfoOverviewPanelClass *klass)
   gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, disk_row);
   gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, gnome_version_row);
   gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, graphics_row);
+  gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, device_model_row);
   gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, hardware_box);
   gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, hostname_editor);
   gtk_widget_class_bind_template_child (widget_class, CcInfoOverviewPanel, hostname_entry);
diff --git a/panels/info-overview/cc-info-overview-panel.ui b/panels/info-overview/cc-info-overview-panel.ui
index de377bb69..e2d18c48b 100644
--- a/panels/info-overview/cc-info-overview-panel.ui
+++ b/panels/info-overview/cc-info-overview-panel.ui
@@ -68,6 +68,15 @@
                       <class name="frame" />
                     </style>
 
+                    <!-- Device Model -->
+                    <child>
+                      <object class="CcListRow" id="device_model_row">
+                        <property name="visible">False</property>
+                        <property name="activatable">False</property>
+                        <property name="title" translatable="yes">Model</property>
+                      </object>
+                    </child>
+
                     <!-- Memory -->
                     <child>
                       <object class="CcListRow" id="memory_row">
-- 
2.26.1

